<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('free_text')->nullable();
            $table->text('drive_times')->nullable();
            $table->enum('type',['block','weekly'])->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('locked')->default(0);
            $table->enum('role',['null','driver','passenger','admin'])->default('null');
            $table->integer('town_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
