<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/legal', 'HomeController@legalNotice');
Route::get('success/{hash}', ['as' => 'osp.mail', 'uses' => 'HomeController@emailOkay']);
Route::group(['prefix' => 'test'], function () {
    Route::get('maps', ['as' => 'test.gm', 'uses' => 'HomeController@testGM']);
});
Route::group(['middleware' => 'auth'], function () {

    Route::get('account', ['as' => 'osp.accountShow', 'uses' => 'HomeController@userDetail']);
    Route::post('account', ['as' => 'osp.accountSave', 'uses' => 'HomeController@userDetailSave']);
    Route::get('contact/{reciver}', ['as' => 'osp.contactShow', 'uses' => 'HomeController@contactShow']);
    Route::post('contact/{reciver}', ['as' => 'osp.contact', 'uses' => 'HomeController@contactSend']);

    Route::get('town/{townId}/{role?}', ['as' => 'osp.town', 'uses' => 'HomeController@town']);
    Route::group(['prefix' => 'backend', 'middleware' => 'backend'], function () {
        Route::get('/', ['as' => 'osp.backend.home', 'uses' => 'BackendController@index']);
        Route::get('/{user_id}/toggleLock', ['as' => 'osp.backend.toggleLock', 'uses' => 'BackendController@toogleLock']);
    });
});

