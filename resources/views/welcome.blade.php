@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Testing</div>

                    <div class="panel-body">
                        <ul>
                            <li><a href="{{ route('test.gm') }}">Google Maps API</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection