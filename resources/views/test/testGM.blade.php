@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(session()->has('register_success'))
                            <div class="alert alert-success">
                                Vielen Dank für Deine Registrierung. Bitte bestätige Deine E-Mail-Adresse
                            </div>
                        @endif
                        <form method="POST" id="testform">
                            <div class="row">
                                <div class="form-group col-lg-4 col-md-4" id="zipDiv">
                                    <label>Postleitzahl:</label>
                                    <input type="text" name="zip" id="zip" placeholder="PLZ" class="form-control">
                                    <div class="help-block" id="zipBlock"></div>
                                </div>
                                <div class="form-group col-lg-6 col-md-6">
                                    <label>Ich suche:</label>
                                    <select class="form-control" name="search" id="search">
                                        <option value="driver">Fahrer</option>
                                        <option value="passenger">Mitfahrer</option>
                                        <option value="all" selected>Fahrer und Mitfahrer</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-2 col-md-2">
                                    <label>&nbsp;</label>
                                    <input type="submit" name="test" id="test" class="btn btn-success" value="Suchen" style="margin-top:23px;">
                                </div>
                            </div>
                        </form>
                        <div style="height:300px; margin-top:15px;">
                            <div id="map" style="height:300px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
<script>
    var map;
    var markers = [];
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            @if(request()->has('easter') && request()->get('easter') == 'egg')
            styles: [{
                "featureType": "administrative.neighborhood",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{"color": "#ff478f"}, {"saturation": -58}, {"lightness": 83}]
            }, {
                "featureType": "landscape",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#ff478f"}, {"lightness": 73}]
            }, {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "poi.attraction",
                "elementType": "labels.text",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "poi.business",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"hue": "#3bff00"}, {"color": "#ff478f"}, {"visibility": "on"}, {"weight": 0.6}, {"lightness": 58}]
            }, {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text",
                "stylers": [{"color": "#808080"}, {"hue": "#ff0000"}, {"saturation": 50}, {"lightness": -13}, {"visibility": "off"}]
            }, {
                "featureType": "transit",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#ff478f"}, {"lightness": 73}]}]
            @endif
        });
        var image = {
            url: '/img/gso-bk-logo.jpg',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(50, 100),
        };
        var marker = new google.maps.Marker({
            position: {'lat': 50.9275832, 'lng': 6.9956184},
            map: map,
            title: 'GSO-Köln',
            icon: '/img/university.png'
        });

        map.setCenter({'lat': 50.9275832, 'lng': 6.9956184});
        var contentString = '<div id="content">' +
                '<div id="siteNotice">' +
                '</div>' +
                '<h4 id="firstHeading" class="firstHeading">GSO-Köln</h4>' +

                '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });


        $.get('/api/geo')
                .done(function (data) {
                    $.each(data, function (key, value) {
                        addMarker(value);
                        console.log(value);
                    });
                });
    }
    function addMarker(myLatLng) {

        if ($('#search').val() === 'driver') {
            if (myLatLng.avaible_drivers == 0) {
                return;
            }
        }
        if ($('#search').val() === 'passenger') {
            if (myLatLng.avaible_passengers == 0) {
                return;
            }
        }
        if (myLatLng.avaible_drivers == 0 && myLatLng.avaible_passengers == 0) {
            return;
        }
        var contentString = '<div id="content">' +
                '<div id="siteNotice">' +
                '</div>' +
                '<h4 id="firstHeading" class="firstHeading">' + myLatLng.name + '(' + myLatLng.zip + ')</h4>' +
                '<div id="bodyContent">' +
                '<p>';
        if (myLatLng.avaible_drivers != undefined || myLatLng.avaible_drivers != 0) {
            contentString += 'Anzahl Fahrer:' + myLatLng.avaible_drivers + '' +
                    '<br /> ';
        }
        if (myLatLng.avaible_passengers != undefined || myLatLng.avaible_passengers != 0) {
            contentString += 'Anzahl Mitfahrer: ' + myLatLng.avaible_passengers + '' +
                    '<br />';
        }

        @if(Auth::check())
                contentString += '<a href="' + myLatLng.route + '">Für mehr Informationen klicken </a>';
        @else
                contentString += '<a href="' + myLatLng.route + '">Für mehr Informationen anmelden </a>';
        @endif
                contentString +=
                '</p>' +
                '</div>' +
                '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });


        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: myLatLng.formatted_address
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
        markers.push(marker);

    }
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }
    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }
    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }
    $('#test').on('click', function (e) {
        $('#zipDiv').removeClass('has-error');
        $('#zipBlock').html('');
        $.post('/api/geo', $("#testform").serialize())
                .done(function (data) {
                    if (data.error != undefined) {
                        $('#zipDiv').addClass('has-error');
                        $('#zipBlock').html('<strong>' + data.error + '</strong>');
                    } else {
                        deleteMarkers();
                        $.each(data, function (key, value) {
                            addMarker(value);
                            if (value.zip == $("#zip").val()) {
                                map.setCenter(value);
                            }

                            console.log(value);
                        });
                    }
                });

        e.preventDefault();
    });

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_GEOCODING_API_KEY') }}&signed_in=false&callback=initMap"></script>
@endpush