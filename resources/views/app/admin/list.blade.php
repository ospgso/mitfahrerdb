@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(session()->has('success_toggleLocked'))
                            <div class="alert alert-success">
                                Der Nutzer {{ session()->get('success_toggleLocked')->name }} wurde
                                erfolgreich @if(session()->get('success_toggleLocked')->locked) entsperrt @else
                                    gesperrt @endif .
                            </div>
                        @endif
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nickname</th>
                                <th>E-Mail</th>
                                <th>Angebot</th>
                                <th>Unterrichtstyp</th>
                                <th>Status</th>
                                <th>Aktion</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>@if($user->role == 'driver') Fahrer @elseif($user->role == 'admin') Admin @else Mitfahrer @endif</td>
                                    <td>@if($user->type == 'block') Block @else Wöchentlich @endif</td>
                                    <td>@if($user->locked == 1) <span class="danger">Gesperrt</span> @else <span
                                                class="success">Nicht gesperrt</span> @endif</td>
                                    <td>
                                        <a href="{{ route('osp.backend.toggleLock',['user_id' => $user->id]) }}">@if($user->locked == 1)
                                                Entsperren @else Sperren @endif</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection