@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(session()->has('success') && session()->get('success') == 'true')
                            <div class="alert alert-success">
                                Ihre Kontaktanfrage wurde versendet!
                            </div>
                        @endif
                        <form method="POST" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="reciver" value="{{ $reciver }}">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Freitext:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="message"></textarea>
                                    <span class="form-control-static">Mit freundlichen Grüßen <br /> {{ Auth::user()->name }}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Absenden</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection