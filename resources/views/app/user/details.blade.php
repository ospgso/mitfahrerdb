@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(session()->has('success') && session()->get('success') == 'true')
                            <div class="alert alert-success">
                                Ihre Daten wurden gespeichert.
                            </div>
                        @endif
                        <form method="POST" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" id="inputEmail3"
                                           value="{{ $user->name }}" readonly disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Postleitzahl</label>
                                <div class="col-sm-10">
                                    <input type="text" name="zip" class="form-control" id="inputEmail3"
                                           value="{{ $user->town->zip }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">E-Mail</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" id="inputEmail3"
                                           value="{{ $user->email }}" readonly disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Ich bin</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="role">


                                        <option value="null" @if($user->role == 'null') selected @endif>Garnichts
                                        </option>
                                        <option value="driver" @if($user->role == 'driver') selected @endif>Fahrer
                                        </option>
                                        <option value="passenger" @if($user->role == 'passenger') selected @endif>
                                            Mitfahrer
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Ich habe</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="type">
                                        <option value="null" @if($user->type == null) selected @endif disabled>Keine Angabe
                                        </option>
                                        <option value="block" @if($user->type == 'block') selected @endif>Blockunterricht
                                        </option>
                                        <option value="weekly" @if($user->role == 'weekly') selected @endif>
                                            Wöchentlichen Unterricht
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Freitext:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="free_text">{{ $user->free_text}}</textarea>
                                    <span class="help-block">Gebe hier z.B. Informationen an wie viele Leute du mitnehmen kannst.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Deine Schulzeiten:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="drive_times">{{ $user->drive_times}}</textarea>
                                    <span class="help-block">Gebe hier z.B. die Tage in denen du an der Schule bist ein.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Speichern</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection