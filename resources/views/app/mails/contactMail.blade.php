@extends('layouts.email')

@section('content')
    <!-- Greeting -->
    <h1 style="margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;">
        Hallo {{ $user->name }},
    </h1>
    <p>
        Sie haben eine Kontaktanfrage von {{ $sender->name }} erhalten:
    </p>
    <p>{{ $text }}</p>
@endsection