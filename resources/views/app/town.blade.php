@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">
                    <div class="panel-body">
                        @forelse($users as $user)
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-3">
                                    <em>{{ $user->name }}</em> <br/>

                                    <em>aus ({{ $user->town->zip }}) {{ $user->town->name }}</em>
                                    <br/>
                                    @if($user->type == 'block')
                                        <strong>Block Schule</strong>
                                    @else
                                        <strong>Wöchentlich Schule</strong>
                                    @endif
                                    <br/>
                                    <strong>Ich suche:</strong> @if($user->role == 'driver') Mitfahrer @else
                                            Fahrer @endif
                                    <br/>
                                    <br/>
                                    <strong>Zeiten:</strong>{{ $user->drive_times }}

                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                    {{ $user->free_text }}
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    @if(Auth::check())
                                        <a href="{{ route('osp.contactShow',['reciver' => $user->id]) }}">Kontakt
                                            aufnehmen</a>
                                    @else
                                        <a href="{{ route('login') }}">Login</a>
                                    @endif
                                </div>
                            </div>
                            <hr>
                        @empty
                            <div class="alert alert-danger">Keine Nutzer vorhanden</div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection