<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <link href="/css/app.css" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <style>
        .back {
            background-color: pink;
        }
    </style>
</head>
<body>
<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" rel="home" href="{{ url('/') }}" title="Buy Sell Rent Everyting">
                <img style="max-width:100px; margin-top: -7px;"
                     src="/img/gso-bk-logo.png">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}">Startseite</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Anmelden</a></li>
                    <li><a href="{{ url('/register') }}">Registrieren</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('osp.accountShow') }}">
                                    <i class="glyphicon glyphicon-user"></i> Mein Account
                                </a>
                            </li>
                            @if(Auth::user()->role == 'admin')
                                <li>
                                    <a href="{{ route('osp.backend.home') }}">
                                        <i class="glyphicon glyphicon-cog"></i> Admin Bereich
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <i class="glyphicon glyphicon-log-out"></i> Abmelden
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>

                        </ul>
                    </li>
                @endif
                <li><a href="{{ url('/legal') }}">Impressum</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container">
    @yield('content')
    <div class="row">
        <div class="col-md-offset-5 col-md-3 col-lg-offset-5 col-lg-3">
            <span class="help-block">{{ number_format(\App\Offer::all()->count(),0,',','.')}}
                vermittelte Kontakte</span>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>

<script src="//cdn.rawgit.com/namuol/cheet.js/master/cheet.min.js"
        type="text/javascript"></script>
<script src="/js/app.js"></script>
<script>
    cheet('c h e e t', function () {
        $('div').each(function (index) {
            console.log(index);
            setInterval(
                    $(this).css({
                        '-webkit-transform': 'transform 4s ease',
                        '-webkit-transform': 'rotate(360deg)',
                        '-moz-transform': 'transform 4s ease',
                        '-moz-transform': 'rotate(360deg)',
                        '-ms-transform': 'transform 4s ease',
                        '-ms-transform': 'rotate(360deg)',
                        'transition': 'transform 4s ease',
                        'transform': 'rotate(360deg)',
                    })
                    , 500);
        });
        $('body').toggleClass('back');
        setInterval(function () {
            $('body').toggleClass('back');
        }, 500);

        console.log('Cheeting');
    });
    cheet('b a r r e l', function () {
        $('body').css({
            '-webkit-transform': 'transform 2s ease',
            '-webkit-transform': 'rotate(360deg)',
            '-moz-transform': 'transform 2s ease',
            '-moz-transform': 'rotate(360deg)',
            '-ms-transform': 'transform 2s ease',
            '-ms-transform': 'rotate(360deg)',
            'transition': 'transform 2s ease',
            'transform': 'rotate(360deg)',
        });

        console.log('Barrel Role');
    });
</script>
@stack('js')

</body>
</html>
