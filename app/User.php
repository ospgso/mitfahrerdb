<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','town_id','locked','emailHash'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'locked' => 'boolean',
        'active' => 'boolean'
    ];

    public function scopeNotLocked($query){
        $query->where('locked','=',0);
    }
    public function scopeActive($query){
        $query->where('active','=',1);
    }
    public function town(){
        return $this->belongsTo(Town::class);
    }
    public function offers(){
        return $this->hasMany(Offer::class);
    }
}
