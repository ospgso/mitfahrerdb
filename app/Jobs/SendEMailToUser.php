<?php

namespace App\Jobs;

use App\Mail\ContactEMail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendEMailToUser
 * @package App\Jobs
 */
class SendEMailToUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var User
     */
    public $user;
    /**
     * @var User
     */
    public $sender;
    /**
     * @var string
     */
    public $text;


    /**
     * SendEMailToUser constructor.
     * @param User $user
     * @param string $text
     */
    public function __construct(User $user, User $sender, $text)
    {
        $this->user = $user;
        $this->sender = $sender;
        $this->text = $text;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->email)->send(new ContactEMail($this->user, $this->sender, $this->text));
    }
}
