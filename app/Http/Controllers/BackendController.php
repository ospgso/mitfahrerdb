<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;

class BackendController extends Controller
{
    public function index()
    {
        $users = User::active()->get();
        return view('app.admin.list', compact('users'));
    }

    public function toogleLock($user_id){
        $user = User::find($user_id);

        $user->locked = !$user->locked;
        $user->save();

        return back()->with('success_toggleLocked',$user);

    }
}
