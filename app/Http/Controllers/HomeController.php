<?php

namespace App\Http\Controllers;

use App\Jobs\SendEMailToUser;
use App\Offer;
use App\Town;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Geocoder\Google\Geocoder;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test.testGM');
    }

    public function testGM()
    {
        return view('test.testGM');
    }

    public function geoApi(Request $request)
    {
        if ($request->has('search')) {
            $search = $request->get('search');
            return Town::all()->each(function (&$e) use ($search) {
                if (in_array($search, ['all', 'driver'])) {
                    $e->avaible_drivers = $e->users()->active()->notLocked()->where('role', '=', 'driver')->count();
                    $e->route = route('osp.town', ['townId' => $e->id, 'role' => 'driver']);
                }
                if (in_array($search, ['all', 'passenger'])) {
                    $e->avaible_passengers = $e->users()->active()->notLocked()->where('role', '=', 'passenger')->count();
                    $e->route = route('osp.town', ['townId' => $e->id, 'role' => 'passenger']);
                }

            });
        } else {
            return Town::all()->each(function (&$e) {
                $e->avaible_drivers = $e->users()->active()->notLocked()->where('role', '=', 'driver')->count();
                $e->avaible_passengers = $e->users()->active()->notLocked()->where('role', '=', 'passenger')->count();
                $e->route = route('osp.town', ['townId' => $e->id, 'role' => '']);
            });
        }
    }

    public function geoApiPost(Request $request)
    {
        $plz = $request->get('zip');
        $town = Town::where('zip', '=', $plz)->first();
        $validator = Validator::make($request->all(), [
            'zip' => 'required|regex:/\b\d{5}\b/',
        ]);
        if ($validator->fails() == false) {
            if ($town === null) {
                $geoCoder = new Geocoder(new Client());
                $geoCoder->setApiKey(env('GOOGLE_MAPS_GEOCODING_API_KEY'));
                $_plz = 'DE-' . $plz;
                $coords = $geoCoder->getCoordinatesForQuery("$_plz");
                if ($coords['accuracy'] != 'NOT_FOUND') {
                    $name = explode(',', $coords['formatted_address']);
                    $town = Town::create(['zip' => $plz, 'name' => str_replace($plz, '', $name[0]), 'lat' => $coords['lat'], 'lng' => $coords['lng']]);
                }
            }
            return $this->geoApi($request);
        } else {
            return response()->json(['error' => 'PLZ ungültig']);
        }
    }

    public function town($id, $role = null)
    {
        if ($role == null) {
            $users = User::where('town_id', '=', $id)->notLocked()->get();
        } else {
            $users = User::where('town_id', '=', $id)->where('role', '=', $role)->notLocked()->get();
        }
        return view('app.town', compact('users'));

    }

    public function contactShow($reciver)
    {
        return view('app.contactShow', compact('reciver'));
    }

    public function contactSend(Request $request)
    {
        $user = User::find($request->get('reciver'));
        $offer = new Offer();
        $offer->town_id = $user->town_id;
        $offer->save();
        $this->dispatch(new SendEMailToUser($user, Auth::user(), $request->get('message')));
        return back()->with('success', true);
    }

    public function userDetail()
    {
        $user = Auth::user();
        return view('app.user.details', compact("user"));
    }

    public function userDetailSave(Request $request)
    {
        $user = Auth::user();
        $user->role = ($request->get('role') == 'admin') ? 'driver' : $request->get('role');
        $user->type = $request->get('type');
        $user->free_text = $request->get('free_text');
        $user->drive_times = $request->get('drive_times');
        $plz = $request->get('zip');
        $town = Town::where('zip', '=', $plz)->first();
        if ($town === null) {
            $geoCoder = new Geocoder(new Client());
            $geoCoder->setApiKey(env('GOOGLE_MAPS_GEOCODING_API_KEY'));
            if (is_numeric($plz)) {
                $_plz = 'DE-' . $plz;
            } else {
                $_plz = $plz;
            }
            $coords = $geoCoder->getCoordinatesForQuery("$_plz");
            if ($coords['accuracy'] != 'NOT_FOUND') {
                $name = explode(',', $coords['formatted_address']);
                $town = Town::create(['zip' => $plz, 'name' => str_replace($plz, '', $name[0]), 'lat' => $coords['lat'], 'lng' => $coords['lng']]);
            } else {
                return redirect()->back()->withErrors(['zip' => 'Postleitzahl ungültig']);
            }
        } else {
            $user->town_id = $town->id;
        }
        $user->save();
        return back()->with('success', true);
    }

    public function emailOkay($hash)
    {
        $user = User::where('emailHash', '=', $hash)->first();
        if ($user == null) {
            return redirect("/");
        } else {
            if ($user->active == 1) {
                return redirect("/");
            } else {
                $user->active = 1;
                $user->emailHash = null;
                $user->save();
                return redirect()->to(route('login'))->with('active', true);
            }
        }
    }

    public function legalNotice(){
        return view('app.legalNotice');
    }
}
