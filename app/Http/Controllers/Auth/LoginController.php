<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        $redirect = ($request->user()->role == null) ? redirect()->intended($this->redirectPath()) : redirect()->to(route('osp.accountShow'));
        return $this->authenticated($request, $this->guard()->user()) ?: $redirect;
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            if ($this->guard()->user()->active == true) {
                if ($this->guard()->user()->locked == true) {
                    $this->logout();
                    if (!$lockedOut) {
                        $this->incrementLoginAttempts($request);
                    }
                    return redirect()->back()->withInput($request->only($this->username(), 'remember'))->withErrors([$this->username() => 'Ihr Account wurde gesperrt.']);

                }
                return $this->sendLoginResponse($request);
            } else {


                $this->logout();
                if (!$lockedOut) {
                    $this->incrementLoginAttempts($request);
                }
                return redirect()->back()->withInput($request->only($this->username(), 'remember'))->withErrors([$this->username() => 'Bitte aktivieren Sie Ihren Account.']);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if (!$lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }
}
