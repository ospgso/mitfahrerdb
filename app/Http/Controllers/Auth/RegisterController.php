<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\UserRegisterd;
use App\Town;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Spatie\Geocoder\Google\Geocoder;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $plz = $data['zip'];

        if (Town::where('zip', '=', $plz)->orWhere('name', 'LIKE', $plz)->first() instanceof Town) {
        } else {
            $geoCoder = new Geocoder(new Client());
            $geoCoder->setApiKey(env('GOOGLE_MAPS_GEOCODING_API_KEY'));

            $_plz = 'DE-' . $plz;

            $coords = $geoCoder->getCoordinatesForQuery("$_plz");
            if ($coords['accuracy'] != 'NOT_FOUND') {
                $name = explode(',', $coords['formatted_address']);
                Town::create(['zip' => $plz, 'name' => str_replace($plz, '', $name[0]), 'lat' => $coords['lat'], 'lng' => $coords['lng']]);
            } else {
                return redirect()->back()->withErrors(['zip' => 'Postleitzahl ungültig']);

            }
        }
        return Validator::make($data, [
            'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users|gsomail',
            'password' => 'required|min:6|confirmed',
            'zip' => 'required|exists:towns,zip|regex:/\b\d{5}\b/',
            'agb' => 'required|accepted'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $town = Town::where('zip', '=', $data['zip'])->first();
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'town_id' => $town->id,
            'emailHash' => str_random(50)
        ]);

        $user->notify(new UserRegisterd($user));
        return $user;

    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $this->create($request->all());
        $this->guard()->logout();
        return redirect($this->redirectPath())->with('register_success', true);
    }
}
