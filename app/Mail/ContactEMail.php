<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var User
     */
    public $user;
    /**
     * @var User
     */
    public $sender;
    /**
     * @var string
     */
    public $text;


    /**
     * SendEMailToUser constructor.
     * @param User $user
     * @param string $text
     */
    public function __construct(User $user, User $sender, $text)
    {
        $this->user = $user;
        $this->sender = $sender;
        $this->text = $text;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->replyTo($this->sender->email,$this->sender->name);
        $this->from('noreply@osp.lk-development.com','GSO Drive By');
        return $this->view('app.mails.contactMail');
    }
}
