<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public function towns(){
        return $this->hasMany(Town::class);
    }
}
